import { config } from "./config/config-TimeCardBot"; 
import { TimeCardBot } from "./Bot/TimeCardBot";

var bot = new TimeCardBot(config.bot);
bot.start();