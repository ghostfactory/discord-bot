require('babel-core/register');
// require("./main.js");

const defaultBotName = "FunTimeBot";

var fs = require("fs");
var CLI = require("command-line-args");
var cli = CLI([
    { name: "bot", alias: "b", type: String }
]);
var args = cli.parse();
var botName = args.bot;

try {
    var botFilePath = `./Bot/${botName}.js`;
    var startFilePath = `./start-${botName}.js`;
    var botFile = fs.lstatSync(botFilePath);
    var startFile = fs.lstatSync(startFilePath);
    require(startFilePath);

    console.log(`Running as: ${botName}`);
} catch(e) {
    try {
        var botFilePath = `./Bot/${defaultBotName}.js`;
        var startFilePath = `./start-${defaultBotName}.js`;
        var botFile = fs.lstatSync(botFilePath);
        var startFile = fs.lstatSync(startFilePath);
        require(startFilePath);

        console.log(`Bot ${botName} not found. Running as default bot: ${defaultBotName}`);
    } catch(e) {
        console.log(`Bot ${botName} not found. Default bot ${defaultBotName} also not found... wat.`);
        console.log(e);
    }
}