"use strict";

var _ = require("underscore");
import { Util } from "./../Utilities/Util";

const defaultConfig = {
    prefix: "!",
    removeCommandString: false
};

export class CommandHandler 
{
    constructor(client, config = defaultConfig) {
        this.client = client;
        this.prefix = config.prefix;
        this.removeCommandString = config.removeCommandString;
        this.modules = [];
    }
    
    get commands() {
        var commands = [];
        this.modules.forEach(m => {
            commands = commands.concat(m.commands);
        });
        return commands;
    }
    
    
    /* ============================================================= *
     * Event Handling
     * ============================================================= */
    
    handle(event) {
        if(this.isSelf(event)) {
            return;
        }
        
        var commandString = this.commandStringFromEvent(event);
        if(commandString == null) {
            return;
        }
        
        var commandParams = this.parse(commandString);
        var command = this.commands.find(c => c.name == commandParams.name);
        if(command == null) {
            return;
        }
        
        command.action(event, commandParams.param, commandParams.args);
        
        if(this.removeCommandString && !event.message.channel.isPrivate) {
            !event.message.delete();
        }
    }
    
    parse(commandString) {
        if(!_.isString(commandString)) {
            console.log("CommandHandler.parse error: 'commandString' must be of type 'String'");
            return;
        }
        
        /* EX: say "Hello, world." channel Testing
            params = {
                name: "say",
                param: "Hello, world.",
                args: {
                    channel: "Testing"
                }
            }
        */
        var args = commandString.match(/(?:[^\s"]+|"[^"]*")+/g);
        var params = {
            name: Util.replaceAll(args[0], '"', ''),
            param: Util.replaceAll(args[1], '"', ''),
            args: {}
        }

        for (var i = 2; i < args.length; i += 2) {
            if (args.length > i + 1) {
                params.args[args[i]] = Util.replaceAll(args[i + 1], '"', '');
            }
        }

        return params;
    }
    
    
    /* ============================================================= *
     * Event Helpers
     * ============================================================= */
    
    commandStringFromEvent(event) {
        var commandString = event.message.content;
        if(!commandString.startsWith(this.prefix)) {
            return null;
        }
        return commandString.replace(this.prefix, "");
    }
    
    isSelf(event) {
        return event.message.author.id === this.client.User.id;
    }
    
    
    /* ============================================================= *
     * Accessors & Mutators
     * ============================================================= */
    
    registerModule(newModule) {
        this.addModules([newModule]);
    }
    
    registerModules(newModules) {
        newModules.forEach(newModule => {
            var existingModule = this.modules.find(m => m.name == newModule.name);
            if(existingModule) {
                console.log(`CommandHandler.addModule error: Module with name '${newModule.name}' already exists.`)
                return;    
            }
            
            this.modules.push(newModule);
        });
    }
    
    unregisterModule(moduleName) {
        this.removeModules([moduleName]);
    }
    
    unregisterModules(moduleNames) {
        moduleNames.forEach(moduleName => {
            this.modules = this.modules.filter(m => m.name != moduleName);
        });
    }
    
}