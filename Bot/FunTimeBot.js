"use strict";

import { Bot } from "./Bot";
import { MusicPlayer } from "./../Modules/Music/MusicPlayer";
import * as Modules from "./../Modules/_index";

export class FunTimeBot extends Bot
{
    constructor(config) {
        super(config);
        
        this.musicPlayer = new MusicPlayer(this.client);
        this.commandHandler.registerModules([
            new Modules.GeneralModule(this),
            new Modules.ChannelModule(this),
            new Modules.MusicModule(this)
        ]);
    }

}