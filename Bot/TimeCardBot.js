"use strict";

var Firebase = require("firebase");
import { Bot } from "./Bot";
import * as Modules from "./../Modules/_index";

const REF = {
    connectionLog: "connection_log"    
};

const STATUS = {
    online: "online",
    offline: "offline"
}

export class TimeCardBot extends Bot
{
    constructor(config) {
        super(config);
        
        this.firebase = new Firebase(config.firebase);
        this.onlineUsers = [];

        this.commandHandler.registerModules([
            new Modules.GeneralModule(this),
            new Modules.TimeCardModule(this),
        ]);
    }
    
    start() {
        super.start();
        
        this.onGatewayReady(e => {
            this.client
        })
        
        this.onPresenceUpdate(e => {
            var user = e.user;
            if(user.previousStatus !== user.status) {
                this.trackStatus(e);
            }
        });
    }
    
    trackStatus(event) {
        var userId = event.user.id;
        var guildId = event.guild.id;
        var ref = this.firebase.child(`${REF.connectionLog}/${guildId}/${userId}`);
        ref.push(this.createUserData(event));
    }
    
    
    /* ============================================================= *
     * Factories
     * ============================================================= */

     createUserData(event) {
         return {
             status: event.user.status,
             date: new Date().getTime()
         }
     }

}