"use strict";

var Discordie = require("discordie");
import { CommandHandler } from "./CommandHandler";
import { Util } from "./../Utilities/Util";

export class Bot 
{
    constructor(config) {
        this.config = config;
        
        this.client = new Discordie();
        this.commandHandler = new CommandHandler(this.client, config.commands);
    }
    
    
    /* ============================================================= *
     * ACCESSORS & MUTATORS
     * ============================================================= */
    
    get user() {
        return this.client.User;
    }
    
    get currentVoiceChannel() {
        if(this.client.VoiceConnections.length == 0) {
            return null;
        }
        var voiceConnectionInfo = this.client.VoiceConnections[0];
        return voiceConnectionInfo.voiceConnection.channel;
    }
    
    
    /* ============================================================= *
     * LIFE CYCLE
     * ============================================================= */
    
    start() {
        this.client.connect(this.config);
        
        this.onGatewayReady(e => {
            console.log("Connected as: " + this.client.User.username);
        });
        
        this.onMessageCreate(e => {
           this.commandHandler.handle(e);
        });
    }
    
    onEvent(eventName, callback) {
        this.client.Dispatcher.on(eventName, callback);
    }
    
    
    /* ============================================================= *
    * CHANNEL HELPERS
    * ============================================================= */

    joinVoiceChannelInGuild(channelName, guild) {
        var channel = guild.voiceChannels.find(c => c.name.toLowerCase() === channelName.toLowerCase());
        if(!channel) {
            return Util.rejectedPromise(`Could not find voice channel to join in '${guild.name}': ${channelName}`);
        }
        return channel.join();
    }
    
    joinVoiceChannelOfUserInGuild(user, guild) {
        var channel = user.getVoiceChannel(guild);
        if(!channel) {
            return Util.rejectedPromise(`Could not join voice channel of '${user.username}'' in '${guild.name}': Not in a voice channel.`);
        }
        return channel.join();
    }
    
    leaveCurrentVoiceChannel() {
        if(!this.currentVoiceChannel) {
            return Util.rejectedPromise(`Could not leave current voice channel: Not in a voice channel to leave!`);
        }
        return this.currentVoiceChannel.leave();
    }
    
    
    /* ============================================================= *
     * GENERAL HOOKS
     * ============================================================= */
    
    /**
     * Emitted when the `Discordie` instance is ready to use. 
     * All objects except offline members of large guilds (250+ members) will be in cache when this event fires. 
     * You can request offline members using `client.Users.fetchMembers()`. See documentation for `IUserCollection.fetchMembers`.
     */
    onGatewayReady(callback) {
        return this.onEvent("GATEWAY_READY", callback);
    }
    
    /**
     * Emitted when login or gateway auth failed, or primary gateway socket disconnects, closing all open sockets. 
     * Not emitted if disconnected using `client.disconnect()`.
     */
    onDisconnected(callback) {
        return this.onEvent("DISCONNECTED", callback);
    }
    
    
    /* ============================================================= *
     * MESSAGE HOOKS
     * ============================================================= */
     
     /**
      * Emitted when user sends a message.
      */
     onMessageCreate(callback) {
         return this.onEvent("MESSAGE_CREATE", callback);
     }
     
     /**
      * Emitted when user deletes their message. Contains null `message` if not cached.
      */
     onMessageDelete(callback) {
         return this.onEvent("MESSAGE_DELETE", callback);
     }
     
     /**
      * Emitted when user updates their message. Contains null `message` if not cached.
      */
     onMessageUpdate(callback) {
         return this.onEvent("MESSAGE_UPDATE", callback);
     }
     
     /**
      * Emitted when user starts typing.
      */
     onTypingStart(callback) {
         return this.onEvent("TYPING_START", callback);
     }
     
     
     /* ============================================================= *
     * PRESENCE HOOKS
     * ============================================================= */
     
     /**
     * Emitted when on changes for username, avatar, status or game. 
     * Emitted multiple times for each shared guild with the local user and the user presence is for.
     * Compare `user.status` and `user.previousStatus` to detect status changes.
     * Games can be checked with `user.game` and `user.previousGame` (and helpers for names `user.gameName` and `user.previousGameName`) respectively.
     */
    onPresenceUpdate(callback) {
        return this.onEvent("PRESENCE_UPDATE", callback);
    }
    
    /**
     * Emitted when `username`, `avatar` or `discriminator` difference detected in an incoming `PRESENCE_UPDATE` event.
     */
    onPresenceMemberInfoUpdate(callback) {
        return this.onEvent("PRESENCE_MEMBER_INFO_UPDATE", callback);
    }
     
     
    /* ============================================================= *
     * CHANNEL HOOKS
     * ============================================================= */
    
    onChannelCreate(callback) {
        return this.onEvent("CHANNEL_CREATE", callback);
    }
    
    onChannelDelete(callback) {
        return this.onEvent("CHANNEL_DELETE", callback);
    }
    
    onChannelUpdate(callback) {
        return this.onEvent("CHANNEL_UPDATE", callback);
    }
    
    
    /* ============================================================= *
     * VOICE HOOKS
     * ============================================================= */
    
    /**
     * Emitted when a new voice connection is fully initialized.
     */
    onVoiceConnected(callback) {
        return this.onEvent("VOICE_CONNECTED", callback);
    }
    
    /**
     * Emitted when a voice socket disconnects.
     */
    onVoiceDisconnected(callback) {
        return this.onEvent("VOICE_DISCONNECTED", callback);
    }
    
    /**
     * Emitted when user joins voice channel.
     */
    onVoiceChannelJoin(callback) {
        return this.onEvent("VOICE_CHANNEL_JOIN", callback);
    }
    
    /**
     * Emitted when user leaves voice channel. 
     * Fields `newChannelId`/`newGuildId` contain ids that will appear in `VOICE_CHANNEL_JOIN` event that will follow if user has moved to another channel, otherwise null.
     */
    onVoiceChannelLeave(callback) {
        return this.onEvent("VOICE_CHANNEL_LEAVE", callback);
    }
    
    onVoiceUserSelfMute(callback) {
        return this.onEvent("VOICE_USER_SELF_MUTE", callback);
    }
    
    onVoiceUserSelfDeaf(callback) {
        return this.onEvent("VOICE_USER_SELF_DEAF", callback);
    }
    
    onVoiceUserMute(callback) {
        return this.onEvent("VOICE_USER_MUTE", callback);
    }
    
    onVoiceUserDeaf(callback) {
        return this.onEvent("VOICE_USER_DEAF", callback);
    }
    
    
    /* ============================================================= *
     * GUILD HOOKS
     * ============================================================= */
    
    // TODO: GUILD_CREATE
    // TODO: GUILD_DELETE
    // TODO: GUILD_UPDATE
    // TODO: GUILD_MEMBER_ADD
    // TODO: GUILD_MEMBER_REMOVE
    // TODO: GUILD_MEMBER_UPDATE
    // TODO: GUILD_BAN_ADD
    // TODO: GUILD_BAN_REMOVE
    // TODO: GUILD_ROLE_CREATE
    // TODO: GUILD_ROLE_DELETE
    
    /**
     * Emitted when guild becomes unavailable. Guild will be deleted from cache until another `GUILD_CREATE`.
     */
    onGuildUnavailable(callback) {
        return this.onEvent("GUILD_UNAVAILABLE", callback);
    }
    
}