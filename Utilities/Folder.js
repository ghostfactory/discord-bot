"use strict";

export class Folder
{
    constructor(folderName, parentFolder) {
        this.folderName = folderName;
        this.parentFolder = parentFolder;
        this.contents = [];
    }
    
    get fullPath() {
        var parentFullPath = this.parentFolder ? this.parentFolder.fullPath() : "";
        return `${parentFullPath}/${this.folderName}`;
    }
}