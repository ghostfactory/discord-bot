"use strict";

export class File
{
    constructor(fileName, parentFolder) {
        this.fileName = fileName;
        this.parentFolder = parentFolder;
    }
    
    get fullPath() {
        var parentFullPath = this.parentFolder ? this.parentFolder.fullPath() : "";
        return `${parentFullPath}/${this.fileName}`;
    }
}