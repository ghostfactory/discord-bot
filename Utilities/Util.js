"use strict";

var _ = require("underscore");
var fs = require('fs');

import { FILE_TYPES } from "../Modules/Music/MusicPlayer";
import { File } from "./File";
import { Folder } from "./Folder";

export class Util 
{
    static replaceAll(string, find, replace) {
        if(!_.isString(string) || !_.isString(find) || !_.isString(replace)) {
            return null;
        }
        
        return string.replace(new RegExp(Util.escapeRegExp(find), 'g'), replace);
    }
    
    static escapeRegExp(string) {
        if(!_.isString(string)) {
            return null;
        }
        
        return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    }
    
    static getDirContents(dir) {
        var getItems = (path, parentFolder) => {
            var items = fs.readdirSync(path);
            var list = [];

            items.forEach(item => {
                var stat = fs.statSync(`${path}/${item}`);
                
                if(stat.isDirectory()) {
                    var folder = new Folder(item, parentFolder);
                    folder.contents = getItems(`${path}/${item}`, folder);
                    list.push(folder);
                } else if(stat.isFile()) {
                    var fileType = FILE_TYPES.find(ft => item.endsWith(ft));
                    if(!fileType) {
                        return;
                    }
                    
                    var file = new File(item, parentFolder);
                    list.push(file);
                }
            });
            
            return list;
        };
        
        return getItems(dir);
    }
    
    static resolvedPromise(data) {
        return new Promise((resolve, reject) => resolve(data));
    }
    
    static rejectedPromise(errorMessage) {
        console.log(errorMessage);
        return new Promise((resolve, reject) => reject(new Error(errorMessage)));
    }
}