"use strict";

import { Module } from "./../../Bot/Module";
import * as Commands from "./Commands/_index";

export class MusicModule extends Module
{
    constructor(bot) {
        super(bot);
        
        this.commands = [
            new Commands.ListMusicCommand(bot),
            new Commands.PlayMusicCommand(bot),
            new Commands.StopMusicCommand(bot)
        ];
    }
    
    get name() {
        return "Music";
    }
}