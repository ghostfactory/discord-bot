"use strict";

var _ = require("underscore");
var lame = require('lame');
var fs = require('fs');
var path = require('path');

import { Util } from "./../../Utilities/Util";
import { File } from "./../../Utilities/File";
import { Folder } from "./../../Utilities/Folder";

const SOUNDS_PATH = `${path.dirname(require.main.filename)}/music`;
export const FILE_TYPES = [".mp3", ".wav", ".aiff", ".flac", ".m4a", ".ogg", ".oga", ".opus", ".wma", ".webm"];

export class MusicPlayer 
{
    constructor(client) {
        this.client = client;
        
        this.mp3decoder = null;
        this.stopPlaying = false;
    }
    
    get isPlaying() {
        return this.mp3decoder != null;
    }
    
    play(fileName) {
        if(this.mp3decoder) {
            return;
        }
        this.stopPlaying = false;

        return new Promise((resolve, reject) => {
            
            this.getSoundFilesPromise().then(data => {
                var filePath = this.getFilePathFromFileName(fileName, data);
                if(!filePath) {
                    var errorMessage = `Could not find file to play: ${fileName}`;
                    reject(new Error(errorMessage));
                    console.log(errorMessage);
                    return;
                }
                
                this.mp3decoder = new lame.Decoder();
                
                this.mp3decoder.on("format", pcmfmt => {
                    this.decode(pcmfmt);
                });
                
                this.mp3decoder.once("end", () => {
                    this.mp3decoder = null;
                    resolve();
                });

                fs.createReadStream(`${SOUNDS_PATH}/${filePath}`).pipe(this.mp3decoder);
            }).catch(err => {
                reject(err);
            });
            
        });
    }
    
    decode(pcmfmt) {
        // note: Discordie encoder does resampling if rate != 48000
        var options = {
            frameDuration: 60,
            sampleRate: pcmfmt.sampleRate,
            channels: pcmfmt.channels,
            float: false
        };

        var readSize =
            pcmfmt.sampleRate / 1000 *
            options.frameDuration *
            pcmfmt.bitDepth / 8 *
            pcmfmt.channels;

        this.mp3decoder.once("readable", () => {
            if(!this.client.VoiceConnections.length) {
                return console.log("Could not play sound. Voice not connected.");
            }

            var encoder = this.getCurrentEncoder(options);

            const needBuffer = () => encoder.onNeedBuffer();
            encoder.onNeedBuffer = () => {
                if(!this.mp3decoder) {
                    return;
                }
                
                if(this.stopPlaying) {
                    this.mp3decoder.end();
                    this.mp3decoder = null;
                    return;
                }
                
                var chunk = this.mp3decoder.read(readSize);

                // Delay the packet if no data buffered
                if (!chunk) return setTimeout(needBuffer, options.frameDuration);

                var sampleCount = readSize / pcmfmt.channels / (pcmfmt.bitDepth / 8);
                encoder.enqueue(chunk, sampleCount);
            };

            needBuffer();
        });
    }
    
    getCurrentEncoder(options) {
        var voiceConnectionInfo = this.client.VoiceConnections[0];
        var voiceConnection = voiceConnectionInfo.voiceConnection;

        return voiceConnection.getEncoder(options);
    }
    
    getFilePathFromFileName(fileName, files) {
        var fileType = FILE_TYPES.find(ft => fileName.endsWith(ft));
        if(fileType) {
            return files.find(fn => fn === fileName);
        }
        
        return files.find(fn => fn.startsWith(`${fileName}.`));
    }
    
    getSoundFilesPromise() {
        return new Promise((resolve, reject) => {
            fs.readdir(SOUNDS_PATH, (err, data) => {
                if(err) {
                    reject(err);
                    return;
                }
                resolve(data);
            });
        });
    }
    
    getMusicList() {
        return Util.getDirContents(SOUNDS_PATH);
    }
    
}