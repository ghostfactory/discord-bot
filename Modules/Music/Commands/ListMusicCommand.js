"use strict";

import { Command } from "./../../../Bot/Command";
import { File } from "./../../../Utilities/File";
import { Folder } from "./../../../Utilities/Folder";

const DELIMITER = "\n";
const PREFIX = "-";

export class ListMusicCommand extends Command
{
    get name() {
        return "music-list";
    }
    
    action(event, sayString, args = {}) {
        var response = "";
        
        var musicList = this.bot.musicPlayer.getMusicList();
        var unorganizedFolder = new Folder("- Unorganized -");
        
        musicList.forEach(item => {
            if(item instanceof Folder) {
                response += `${this.getFolderString(item)}\n`;
            } else if(item instanceof File) {
                unorganizedFolder.contents.push(item);
            }
        });
        
        response = `${this.getFolderString(unorganizedFolder)}\n` + response;
        
        event.message.channel.sendMessage(response);
    }
    
    getFolderString(folder) {
        var folderString = (folder, prefix) => {
            var theString;
            if(prefix === null) {
                theString = `**${folder.folderName}**\n`;
                theString += "```";
                prefix = "";
            } else {
                theString = `${prefix}[${folder.folderName}]\n`;
                prefix = `${PREFIX}${prefix} `;
            }
            
            theString += folder.contents.map(item => {
                if(item instanceof Folder) {
                    return folderString(item, prefix);
                } else if(item instanceof File) {
                    return `${prefix}${item.fileName}\n`;
                }
            }).join("");
            
            return theString;
        }
        
        return folderString(folder, null) + "```";
    }
}