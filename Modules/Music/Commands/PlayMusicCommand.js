"use strict";

import { Command } from "./../../../Bot/Command";

export class PlayMusicCommand extends Command
{
    get name() {
        return "music-play";
    }
    
    action(event, fileName, args = {}) {
        if(this.bot.musicPlayer.isPlaying) {
            return;
        }
        
        if(!fileName) {
            console.log("Cannot play music. No fileName specified.");
            return;
        }
        
        if(args.channel) {
            return this.joinPlayAndLeaveChannel(fileName, event, args.channel);
        }

        if(!this.bot.client.VoiceConnections.length) {
            return this.joinPlayAndLeaveAuthor(fileName, event);
        }
        
        this.bot.musicPlayer.play(fileName);
    }
    
    joinPlayAndLeaveAuthor(fileName, event) {
        var guild = event.message.guild;
        var user = event.message.author;
    
        this.bot.joinVoiceChannelOfUserInGuild(user, guild).then(() => {
            return this.bot.musicPlayer.play(fileName);
        }).then(() => {
            return this.bot.leaveCurrentVoiceChannel();
        });
    }
    
    joinPlayAndLeaveChannel(fileName, event, channelName) {
        var guild = event.message.guild;
    
        this.bot.joinVoiceChannelInGuild(channelName, guild).then(() => {
            return this.bot.musicPlayer.play(fileName);
        }).then(() => {
            return this.bot.leaveCurrentVoiceChannel();
        });
    }
}