"use strict";

import { Command } from "./../../../Bot/Command";

export class StopMusicCommand extends Command
{
    get name() {
        return "music-stop";
    }
    
    action(event, sayString, args = {}) {
        this.bot.musicPlayer.stopPlaying = true;
    }
}