"use strict";

import { Command } from "./../../../Bot/Command";

export class HelloWorldCommand extends Command
{
    get name() {
        return "hello-world";
    }
    
    action(event, param, args = {}) {
        event.message.channel.sendMessage("Hello, world!");
    }
}