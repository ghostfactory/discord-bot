"use strict";

import { Command } from "./../../../Bot/Command";

export class SayCommand extends Command
{
    get name() {
        return "say";
    }
    
    action(event, sayString, args = {}) {
        var recievingChannels = [event.message.channel];
        
        var guild = event.message.guild;
        var channels = this.bot.client.Channels.forGuild(guild);
        
        if(args.channel) {
            recievingChannels = channels.filter(channel => channel.name == args.channel);
            if(recievingChannels.length == 0) {
                return;
            }
        }
        
        recievingChannels.forEach(channel => channel.sendMessage(sayString));
    }
}