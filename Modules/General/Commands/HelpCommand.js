"use strict";

import { Command } from "./../../../Bot/Command";

export class HelpCommand extends Command
{
    get name() {
        return "help";
    }
    
    action(event, sayString, args = {}) {
        var commandHandler = this.bot.commandHandler;
        
        var responseString = "Available commands: \n\n";
        
        commandHandler.modules.forEach(m => {
            responseString += `**${m.name}** \n`;
            
            var commandNames = m.commands.map(command => {
                return commandHandler.prefix + command.name;
            });
            
            responseString += "```" + commandNames.join("\n") + "```" + "\n";
        });

        event.message.channel.sendMessage(responseString);
    }
}