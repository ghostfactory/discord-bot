"use strict";

import { Module } from "./../../Bot/Module";
import * as Commands from "./Commands/_index";

export class GeneralModule extends Module
{
    constructor(bot) {
        super(bot);
        
        this.commands = [
            new Commands.HelpCommand(bot),
            new Commands.ThanksCommand(bot),
            new Commands.SayCommand(bot),
            new Commands.HelloWorldCommand(bot)
        ];
    }
    
    get name() {
        return "General";
    }
}