"use strict";

import { Module } from "./../../Bot/Module";
import * as Commands from "./Commands/_index";

export class TimeCardModule extends Module
{
    constructor(bot) {
        super(bot);
        
        this.commands = [

        ];
    }
    
    get name() {
        return "Time Card";
    }
}