"use strict";

import { Module } from "./../../Bot/Module";
import * as Commands from "./Commands/_index";

export class ChannelModule extends Module
{
    constructor(bot) {
        super(bot);
        
        this.commands = [
            new Commands.JoinChannelCommand(bot),
            new Commands.LeaveChannelCommand(bot)
        ];
    }
    
    get name() {
        return "Channel";
    }
}