"use strict";

import { Command } from "./../../../Bot/Command";

export class LeaveChannelCommand extends Command
{
    get name() {
        return "channel-leave";
    }
    
    action(event, channelName, args = {}) {
        this.bot.leaveCurrentVoiceChannel();
    }
}