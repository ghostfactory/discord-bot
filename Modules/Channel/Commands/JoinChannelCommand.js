"use strict";

import { Command } from "./../../../Bot/Command";

export class JoinChannelCommand extends Command
{
    get name() {
        return "channel-join";
    }
    
    action(event, channelName, args = {}) {
        var guild = event.message.guild;
        var user = event.message.author;

        if(!channelName) {
            this.bot.joinVoiceChannelOfUserInGuild(user, guild);
        } else {
            this.bot.joinVoiceChannelInGuild(channelName, guild);
        }
    }
}