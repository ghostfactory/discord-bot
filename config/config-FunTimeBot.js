import { secret } from "./secret-FunTimeBot";

export const config = {
    bot: {
        token: secret.bot.token,
        commands: {
            prefix: "!",
            removeCommandString: false
        }
    }
};