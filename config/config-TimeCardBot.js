import { secret } from "./secret-TimeCardBot";

export const config = {
    bot: {
        token: secret.bot.token,
        firebase: "discord-timecardbot.firebaseIO.com",
        commands: {
            prefix: "!",
            removeCommandString: false
        }
    }
};